﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance = null;

    [SerializeField] GameObject player;
    [SerializeField] GameObject click;
    [SerializeField] Animator animator;

    private Vector2 mousePos = new Vector2();


    // ################ STARTER METHODS ################
    void Awake() {
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

    }

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

        Vector2 playerPos = player.transform.position;

        if (Input.GetMouseButtonDown(1)) {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition); //Get mouse position in world

            click.transform.position = mousePos; // Set point to clicked position
            StartCoroutine(ClickAnimation());

            animator.Play("run");

            if (mousePos.x < playerPos.x) {
                player.GetComponent<SpriteRenderer>().flipX = true;
            } else {
                player.GetComponent<SpriteRenderer>().flipX = false;
            }
        }


        // Move
        player.transform.position = Vector3.MoveTowards(player.transform.position, mousePos, 5 * Time.deltaTime);


        if (playerPos == mousePos) {
            animator.Play("idle");
        }
    }

    IEnumerator ClickAnimation() {
        click.SetActive(true);
        click.GetComponent<Animator>().enabled = true;
        yield return new WaitForSeconds(0.3f);
        click.GetComponent<Animator>().enabled = false;
        click.SetActive(false);
    }

}
