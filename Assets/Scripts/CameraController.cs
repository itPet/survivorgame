﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    // Start is called before the first frame update
    void Start() {
        Debug.Log(Mathf.Clamp(10, 1, 5));

    }

    // Update is called once per frame
    void Update() {

        // Current position of the camera
        Vector3 camPos = transform.position;

        // Move the camera
        if (Input.mousePosition.x >= Screen.width - 20) { // 20 is how far from the edge the mouse can be when the camera will start moving.
            camPos.x += 20 * Time.deltaTime;
        }

        if (Input.mousePosition.x <= 20) {
            camPos.x -= 20 * Time.deltaTime;
        }

        if (Input.mousePosition.y >= Screen.height - 20) {
            camPos.y += 20 * Time.deltaTime;
        }

        if (Input.mousePosition.y <= 20) {
            camPos.y -= 20 * Time.deltaTime;
        }

        // Boundaries for camera
        camPos.x = Mathf.Clamp(camPos.x, 0, 16); // if x is less than 0 it will be 0. If greater than 16, it will be 16

        camPos.y = Mathf.Clamp(camPos.y, -9, 0);

        transform.position = camPos;
    }
}
