﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Health : MonoBehaviour
{

    [SerializeField] GameObject gameOverText;

    int health = 100;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DecreaseHealth(GetComponent<Text>()));
        Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition); //Get mouse position in world 
        Physics2D.Raycast(pos, Vector2.zero); //Send ray from that position.
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator DecreaseHealth(Text healthTextComponent) {
        while (health >= 0) {
            yield return new WaitForSeconds(1);
            healthTextComponent.text = "Health: " + health.ToString();
            health--;
        }

        StartCoroutine(GameOver());
    }


    IEnumerator GameOver() {
        gameOverText.SetActive(true);
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("MenuScene");
    }
}
